export class ServerRouteDetails {
  constructor(
    public RouteID: string,
    public Name: string,
    public Direction0: ServerDirection0,
    public Direction1: ServerDirection0,
  ) {}
}

interface ServerDirection0 {
  TripHeadsign: string
  DirectionText: string
  DirectionNum: string
  Shape: ServerShape[]
  Stops: ServerStop[]
}

interface ServerStop {
  StopID: String
  Name: String
  Lon: Number
  Lat: Number
  Routes: String[]
}
interface ServerShape {
  Lon: Number
  Lat: Number
  SeqNum: Number
}

class AppDirection0 {
  constructor(
    public tripHeadSign: string,
    public directionText: string,
    public directionNum: string,
    public shape: AppShape[],
    public stops: AppStop[],
  ) {}
}

export class AppStop {
  constructor(
    public stopId: String,
    public name: String,
    public lon: Number,
    public lat: Number,
    public routes: String[],
  ) {}
}
class AppShape {
  constructor(public lon: Number, public lat: Number, public seqNum: Number) {}
}

export class AppRouteDetails {
  constructor(
    public routeId: String,
    public name: string,
    public direction0: AppDirection0,
    public direction1: AppDirection0,
  ) {}
}
export class RouteDetails {
  static create(event: ServerRouteDetails) {
    return {
      routeId: event.RouteID,
      name: event.Name,
      direction0: new AppDirection0(
        event.Direction0.TripHeadsign,
        event.Direction0.DirectionText,
        event.Direction0.DirectionNum,
        event.Direction0.Shape.map(
          (data: ServerShape) => new AppShape(data.Lat, data.Lon, data.SeqNum),
        ),
        event.Direction0.Stops.map(
          (data: ServerStop) =>
            new AppStop(data.StopID, data.Name, data.Lon, data.Lat, data.Routes),
        ),
      ),
      direction1: new AppDirection0(
        event.Direction1.TripHeadsign,
        event.Direction1.DirectionText,
        event.Direction1.DirectionNum,
        event.Direction1.Shape.map(
          (data: ServerShape) => new AppShape(data.Lat, data.Lon, data.SeqNum),
        ),
        event.Direction1.Stops.map(
          (data: ServerStop) =>
            new AppStop(data.StopID, data.Name, data.Lon, data.Lat, data.Routes),
        ),
      ),
    }
  }
}
