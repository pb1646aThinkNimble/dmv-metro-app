const express = require('express')
const router = express.Router()
const axios = require('axios')

const ENDPOINT = process.env.BACKEND_APP_METRO_BASE
const BUS_ENDPOINT = ENDPOINT + '/Bus.svc/json'
const NEXT_BUS_ENDPOINT = ENDPOINT + '/NextBusService.svc/json'
const BUS_ROUTES_ENDPOINT = BUS_ENDPOINT + '/jRoutes/'
const BUS_ROUTE_DETAILS_ENDPOINT = BUS_ENDPOINT + '/jRouteDetails/'
const BUS_PREDICTION_DETAILS_ENDPOINT = NEXT_BUS_ENDPOINT + '/jPredictions/'

router.get('/bus-routes', async (req, res, next) => {
  const options = {
    headers: { api_key: process.env.VUE_APP_METRO_API },
  }
  try {
    const { data } = await axios.get(`${BUS_ROUTES_ENDPOINT}`, options)
    res.status(200).json({ ...data })
  } catch (e) {
    res.status(400)
  }
})

router.get('/bus-routes/route/:RouteID', async (req, res, next) => {
  let params = req.params

  const options = {
    headers: { api_key: process.env.VUE_APP_METRO_API },
    params: { ...params },
  }
  try {
    const { data } = await axios.get(`${BUS_ROUTE_DETAILS_ENDPOINT}`, options)
    res.status(200).json({ ...data })
  } catch (e) {
    console.log(e)
    res.status(400)
  }
})

router.get('/bus-routes/predictions/:StopID', async (req, res, next) => {
  let params = req.params
  const options = {
    headers: { api_key: process.env.VUE_APP_METRO_API },
    params: { ...params },
  }
  try {
    const { data } = await axios.get(`${BUS_PREDICTION_DETAILS_ENDPOINT}`, options)
    res.status(200).json({ ...data })
  } catch (e) {
    console.log(e)
    res.status(400)
  }
})

module.exports = router
