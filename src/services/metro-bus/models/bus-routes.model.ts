export class ServerRoutes {
  constructor(public RouteID: string, public Name: string, public LineDescription: string) {}
}
export class AppRoutes {
  constructor(public routeId: string, public name: string, public lineDescription: string) {}
}

export class BusRoutes {
  static create(event: ServerRoutes) {
    return {
      routeId: event.RouteID,
      name: event.Name,
      lineDescription: event.LineDescription,
    }
  }
}
