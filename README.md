# dmv-metro

This is a simple DMV-Metro APP written in VueJS with TYPESCRIPT support.
Pushing to develop will use CircleCI to build and test and deploy to heroku under dmv-app-staging.herokuapp.com

**_ note backend errors are just console.logged _**

FOR TL;DR go to ### Final Assessments
FOR TL;DR let me start on a local server skip to ### Project Setup

## Initial Assessments

The first thing to notice is that although the app is set to use typescript you can also include non-typscript components (as with the baseicon component)

Although I would assume the point of including typescript would be to actually use it, there are instances where this may be helpful.
For Example:

1. A Specific integration we'd like to use does not have a typings yet. - While the JS ecosystem is always updating typings and creating them when missing some still do not (as in the case of Stripe) the fix is pretty much simple, we just declare it as a global var with type any, assuming there was no support for non-typed components, this is not the case with VueJS

2. As developers we may have existing projects that are currently not in Typescript, this would allow us to migrate them one piece at a time as in a 'rolling deployment'

3. We may be in a rush to get something out and we still have not got a complete hang of using typescript.

## Difficutlies using TS:

There were some difficulties implementing TS to the vue apps. Firstly Typescript transpiles to 'use strict' (you can turn this feature off in webpack) in its new version, this means that some reactive aspects ( in both Vue and even Angular) are harder to understand. In my attempt at this I came across multiple ways of dealing with this issue.
Previously it was okay to declare a new variable with its type but not initialize that variable. For example,
`let userArray: User[];`
The above variable initialization used to be fine, the variable behind the scenes was initialized as null, but not during development, therefore type support was assumed to be on the type of User as an array.
With the new version of TS this is no longer acceptable, instead the variable needs to be initialized with a value.
`let userArray: User[] = ['user1', 'user2'....]`
while this would be an acceptable solution we would have to initialize each element in the array temporarily which would be a problem if our type also has subtypes
`class User { public firstName: string; public lastName: string; public friends: FriendsList[] } class FreindsList{ public firstName: string; public lastName: string; public favorites: string[] }`

In order to initialize an empty array we would have

```
    let userArray: User[] = [{firstName: '', lastName: '', friends:'{firstName:'', lastName:'', favorites:['']}'}]
```

tedious....

While searching for solutions I came accross a number of options that require defning the variable with an initial value of null or undefined.

As an alternative approach we could initialize a data variable on the class (note we would no longer have to use data as a function as we have access to the class itself)

```
export class TestComponent extends Vue{
    // method 1
    userArray!:User[]   // notice the !


    /*
    This method is called the ***  definitive asignment assertion *** it is by far the quickest and most relevant to vue method I have come across, however it does have it's drawbacks, at least in my experience.
    While it is perfectly  fine to use this method on props property, I have had issues when using it in the data property, as this is the same as declaring it to undefiened it does not render under the vue components (at least not using the inspector) so when data does get passed down it does not exist as a reactive property (not sure if this is true to others but it has been an issue for me)
    Regardless when using this method it is required that you check if the variable exists (i.e is not undefined each time you use it or the compiler will complain ) this.user? someFunction(): null;

    */
   // method:2

    userArray:User[]|null =null

    /*
    this option is similar to the *** definitive assignment assertion *** method, but it explicitly sets the variable to null, it is my goto apporach for data (without getting into how the compiler renders undefined or null it seems like null is more acceptable to the reactive props of Vue)

    Again it is necessary to check if the variable is null or not before assigning it a value or the compiler will complain
    */

   // method:3
    /*
    assigning the type in the constructor also seems to be an option but I find this more useful for classes and interfaces than for any of the reactive components
    */
    class User{
        constructor(username: string){}

        const user = new User("test")
        const username = user.username
    }


    }
```

At this point it is also useful to mention that the reason method3 works fine which is also general in TS is if a variable is initiated with a value TS will infer its type. In other words

        ``` const user='pari'
        ```

will automatically infer user is a string.

### Other kinks

Using these tricks to allow for variable initialization sometimes may cause the type support to not be avaialble. One of the benefits of using TS and other stongly typed langauges is the support we get when assigning varaibles values. For example if a function is set to return type of string, and we try and assign it to a variable of type number we will be notified of the issue immediately rather than when the error occurs.
While this will always work we also (depending on the editor) get type support while typing. Currently if I call an object and then its method the next time I type that object its method is cached and autofill will popup, with TS this will happen regardless using the methods or properties of the type. By declaring variables as null sometimes this doesnt happen.

If a function is used to initiate a variable it must return the same type as that variable, so if we have used some of the tricks mentioned above we will have to declare the function of a type | null. Which may bring up the previous kink.

Previously (at least in my experience not that I researched a final transpilation) typescript always transpiled back to the primitve js type if it existed, meaning....

    ```
        const variable: Number=1;

    ```

While this is perfectly acceptable `Number` here is not a primitive type instead it is a TS interface (interfaces are like classes but used to infer type and cannot be initlialized). Therefore attempting to do
`let newVar = variable + 1;`
would result in an error because type Number interface is not equal to type (class) number (at least not anymore). As a general rule I would recommend using the lowercase data type (typescript will show you that you are using a Type as an interface or a class)

The confusing part comes when trying to declare a prop, technically you can declare a prop type twice in its initalization and it's declaration (there are multiple ways to use Props, Watch and Emit I will be demontrating the TS way using the @ symbol)

    ```
            @Prop({type: Number}) public prop!:number;

    ```

This is perfectly valid seeing as in its declaration ({type: Number}) Vue will compile to type number, and TS will follow type number as well.

### Other helpful information

There are several ways to use ts inside of a component, I prefer to use the class extends but Vue.component({}) is also avaialble.

There are also several ways to decalre props, watchers and emitters.

props can be decalred in the regular vue js way by declaring them in the @Component symbol

@Compoent is similar to the components object that injects components to the curent one.
`@Component{ components:{} props:{} }`

or the TS way which would be using the @ Symbol decorator

`@Prop() public test!:string`

As for emit or watch they too can be delcared as in regular vuejs

            ```
                this.$emit('input', val)
            ```

or more TS

    @Emit('input') functionName(){}  // by if a event name is not included it will emit function name

    @Watch('variable') function(){}

### Final Review

I find that the Vue team has done a wonderful job integrating TS to its workflow, while there are certain kinks, it is a welcome addition and there is a lot of documentation and community help. I beleive that whatever difficulty I had in implementing TS in vue may have been reduced because I already have knowledge of TS but I find that that was balanced out by my inexperience with Vue.

### What I missed

I didnt get a chance to work with Veuter yet and finish up any unit testing. That is coming up!

## Project setup

```

npm install

```

### Compiles and hot-reloads for development

```

npm run serve

```

This will serve the front end in development mode the backend is required to connect to the MetroBus API

### Compiles and minifies for production

```

npm run build

```

If you prefer to run API and Frontend all in one first build files

### Compiles server that connect to API

```

npm run start

```

If you prefer to run API and Frontend all in one first build files.
Here the nodejs server is used to run the files but also act as a pasthrough for the APP so as not to expose
the api key. (CORS is enabled with a wild card so theoretically this doesn't actually protect the API key as
anyone could just run agains the API)
HotReload is not enabled nor is it possible to reload pages in production as the # has not been mapped yet.

### Run your unit tests

```

npm run test:unit

```

Tests are coming up

### Run your end-to-end tests

```

npm run test:e2e

```

### Lints and fixes files

```

npm run lint

```

### env variables

VUE_APP_METRO_API=063cf821671844078a13baaced61287b
BACKEND_APP_METRO_BASE=https://api.wmata.com

\*\*UPDATE TO WHATEVER PORT YOUR API IS RUNNING ON

VUE_APP_METRO_BASE=http://localhost:3000/api/metro

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

```

```
