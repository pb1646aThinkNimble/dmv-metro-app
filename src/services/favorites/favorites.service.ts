import { AppStop } from '../metro-bus/models/route-details.model'

class FavoritesModel {
  constructor(public stops: AppStop[] | null) {}
}

class FavoriteItems {
  constructor() {}
  saveRoute(stop: AppStop) {
    /* eslint-disable no-debugger */
    let stops!: AppStop[]
    if (this.getSavedRoutes()) {
      stops = this.getSavedRoutes()['stops']
    } else {
      stops = []
    }
    stops.push(stop)
    let currentSaved = { stops: stops }

    localStorage.setItem('favorites', JSON.stringify(currentSaved))
  }

  getSavedRoutes() {
    let favorites
    let unParsedFavorites = localStorage.getItem('favorites')
    if (unParsedFavorites) {
      let parsed = JSON.parse(unParsedFavorites)
      /* eslint-disable no-debugger */
      favorites = parsed
      return favorites
    } else {
      return null
    }
  }
  removeFavorite(stop: AppStop): void {
    let favorites
    let unParsedFavorites = localStorage.getItem('favorites')
    if (unParsedFavorites) {
      let parsed = JSON.parse(unParsedFavorites)
      /* eslint-disable no-debugger */
      favorites = parsed
    }
    /* eslint-disable no-debugger */
    if (favorites) {
      let index = favorites['stops'].findIndex((fav: AppStop) => fav.stopId === stop.stopId)
      if (index > 0) {
        favorites['stops'].splice(index, 1)
        localStorage.setItem('favorites', JSON.stringify(favorites))
      } else if (index == 0) {
        favorites['stops'].splice(index, 1)
        localStorage.setItem('favorites', JSON.stringify(favorites))
      }
    }
  }
}
export const favoriteItems = new FavoriteItems()
