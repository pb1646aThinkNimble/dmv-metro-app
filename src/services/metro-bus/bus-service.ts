import axios, { AxiosRequestConfig } from 'axios'
import { BusRoutes, AppRoutes, ServerRoutes } from './models/bus-routes.model'
import { RouteDetails, AppRouteDetails } from './models/route-details.model'
import { AppBusPredictions, AppPredictions, BusPredictions } from './models/bus-predictions.model'

const ENDPOINT = process.env.VUE_APP_METRO_BASE
const BUS_ENDPOINT = ENDPOINT + '/bus-routes'
const BUS_ROUTES_ENDPOINT = BUS_ENDPOINT
const BUS_ROUTE_DETAILS_ENDPOINT = BUS_ENDPOINT + '/route'
const BUS_PREDICTION_DETAILS_ENDPOINT = BUS_ENDPOINT + '/predictions'

class BusService {
  constructor() {}

  async getBusRoutes(): Promise<AppRoutes[]> {
    /*     const options: AxiosRequestConfig = {
      headers: { api_key: process.env.VUE_APP_METRO_API },
    } */
    try {
      const res = await axios.get(`${BUS_ROUTES_ENDPOINT}`)
      /* eslint-disable no-debugger */

      const results = res.data.Routes.map((route: ServerRoutes) => BusRoutes.create(route))

      return results
    } catch (e) {
      console.log(e)
      throw e
    }
  }
  async getRouteDetails(RouteId: string): Promise<AppRouteDetails> {
    /*     const options: AxiosRequestConfig = {
      headers: { api_key: process.env.VUE_APP_METRO_API },
      params: { RouteID: RouteId },
    } */
    try {
      const res = await axios.get(`${BUS_ROUTE_DETAILS_ENDPOINT}/${RouteId}`)
      const results = RouteDetails.create(res.data)
      return results
    } catch (e) {
      console.log(e)

      throw e
    }
  }
  async getNextBusPredictions(stopId: string): Promise<AppBusPredictions> {
    /*     const options: AxiosRequestConfig = {
      headers: { api_key: process.env.VUE_APP_METRO_API },
      params: { StopID: stopId },
    } */
    try {
      const res = await axios.get(`${BUS_PREDICTION_DETAILS_ENDPOINT}/${stopId}`)
      const results = BusPredictions.create(res.data, stopId)
      return results
    } catch (e) {
      console.log(e)

      throw e
    }
  }
}
export const busService = new BusService()
