class ServerBusPredictions {
  constructor(public Predictions: ServerPredictions[], public StopName: string) {}
}
export class AppBusPredictions {
  constructor(public predictions: AppPredictions[], public stopName: string) {}
}

class ServerPredictions {
  constructor(
    public RouteID: string,
    public DirectionText: string,
    public DirectionNum: string,
    public Minutes: number,
    public VehicleID: string,
    public TripID: string,
  ) {}
}

export class AppPredictions {
  constructor(
    public routeId: string,
    public directionText: string,
    public directionNum: string,
    public minutes: number,
    public vehicleId: string,
    public tripId: string,
    public stopId: string,
  ) {}
}

export class BusPredictions {
  static create(event: ServerBusPredictions, stopId: string) {
    return {
      stopName: event.StopName,

      predictions: event.Predictions.map(
        prediction =>
          new AppPredictions(
            prediction.RouteID,
            prediction.DirectionText,
            prediction.DirectionNum,
            prediction.Minutes,
            prediction.VehicleID,
            prediction.TripID,
            stopId,
          ),
      ),
    }
  }
}
