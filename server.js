const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const http = require('http')
const path = require('path')
const metroRoutes = require('./routes/metro-routes')
const debug = require('debug')('node-angular')
const normalizePort = val => {
  var port = parseInt(val, 10)
  if (isNaN(port)) {
    return val
  }
  if (port >= 0) {
    return port
  }
  return false
}

const onError = error => {
  if (error.syscall !== 'listen') {
    throw error
  }
  const bind = typeof addr === 'string' ? 'pipe' + addr : 'port ' + port
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated priviledges')
      process.exit(1)
      break
    case 'EADDRINUSE':
      console.error(bind + ' is already in use')
      process.exit(1)
      break
    default:
      throw error
  }
}

const onListening = () => {
  const addr = server.address()
  const bind = typeof addr === 'string' ? 'pipe' + addr : 'port ' + port
}

const port = process.env.PORT || '3000'
app.set('port', port)
const server = http.createServer(app)
server.on('error', onError)
server.on('listening', onListening)
server.listen(port)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-requested-With, Content-Type, Accept, Authorization',
  )
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS, PUT')

  next()
})

app.use(express.static(__dirname + '/dist/'))

app.get('/.*/', function(req, res) {
  res.sendFile(path.join(__dirname + '/dist/index.html'))
})
app.get('/buses', (req, res, next) => {
  res.redirect('/#buses')
})
app.get('buses', (req, res, next) => {
  res.redirect('/#dashboard')
})

app.use('/api/metro', metroRoutes)
